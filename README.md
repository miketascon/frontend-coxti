## Instalación de dependencias
npm install

## Descripción

Proyecto construido con la versión N° 11 De Angular

El proyeto de angular se conecta a un API construido con node.js y express.

La apicación registra usuarios e inica sesión de los mismos.

El usuario puede actualizar su perfil, cambiar su contraseña y también eliminar la cuenta si asi lo desea. Se utilizo SweetAlert para la creación de alertas y como templete sb-Admin para el diseño de la aplicación.

Se empleo la carga de archivos peresoza y se dividio estructuralmente los componentes


# FrontendCoxti

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
