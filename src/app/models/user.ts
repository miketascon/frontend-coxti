export class User {

  public id: string | undefined;
  public nombres: string | undefined;
  public apellidos: string | undefined;
  public email: string | undefined;
  public identificacion: string | undefined;
  public celular: string | undefined;
  public departamento: string | undefined;
  public ciudad: string | undefined;
  public barrio: string | undefined;
  public direccion: string | undefined;
  public salario: string | undefined;
  // tslint:disable-next-line:variable-name
  public otros_ingresos: string | undefined;
  // tslint:disable-next-line:variable-name
  public gastos_mensuales: string | undefined;
  // tslint:disable-next-line:variable-name
  public gastos_financieros: string | undefined;
  // tslint:disable-next-line:variable-name
  public create_at: string | undefined;

  // response
  public message: string | undefined;
  public success: string | undefined;
  public token: string | undefined;

  public results: User[] = [];
  public result: Result | undefined;
  public err: Err | undefined;
}


export interface Result {
    fieldCount: number;
    affectedRows: number;
    insertId: number;
    serverStatus: number;
    warningCount: number;
    message: string;
    protocol41: boolean;
    changedRows: number;
}

export interface Err {
  code: string;
  errno: number;
  sqlMessage: string;
  sqlState: string;
  index: number;
  sql: string;
  level: string;
  timestamp: Date;
}
