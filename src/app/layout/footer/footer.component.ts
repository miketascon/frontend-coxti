import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {

  public anio = 0;

  constructor() { }

  ngOnInit(): void {
    this.anio = new Date().getFullYear();
  }

}
