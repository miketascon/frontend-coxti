import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';

declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

  public user: User = new User();

  constructor() { }

  ngOnInit(): void {
    $('#sidebarToggle').on('click', (e: any) => {
      e.preventDefault();
      $('body').toggleClass('sb-sidenav-toggled');
    });
    this.user = JSON.parse(String(localStorage.getItem('currentUser')));
  }


  logout(): void {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('userID');
    window.location.replace('login');
  }

}
