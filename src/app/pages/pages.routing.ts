import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { PagesComponent } from './pages.component';
import { PerfilComponent } from './perfil/perfil.component';



const routes: Routes = [
  {
    path: '', component: PagesComponent, children: [
      {
        path: 'perfil',
        loadChildren: () => import('./perfil/perfil.module').then((m) => m.PerfilModule),
      },
      {
        path: 'settings',
        loadChildren: () => import('./settings/settings.module').then((m) => m.SettingsModule),
      },
      {
        path: 'rangos',
        loadChildren: () => import('./rangos/rangos.module').then((m) => m.RangosModule),
      },
    ]
  }

];


@NgModule({
  imports: [
   RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
