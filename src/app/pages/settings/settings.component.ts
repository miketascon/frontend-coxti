import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
})
export class SettingsComponent implements OnInit {
  public form: FormGroup;

  constructor(private fb: FormBuilder, private userService: UserService) {
    this.form = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
      passwordConfirm: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  ngOnInit(): void {}

  // tslint:disable-next-line:typedef
  get f() {
    return this.form.controls;
  }

  updatePass(): void {
    this.userService.updatePassword(this.form.value.password, String(localStorage.getItem('userID')))
    .subscribe( data => {
      if (data.success && data.message === 'operación exitosa') {
        Swal.fire({
          title: 'Felicitacione!',
          text: data.message,
          icon: 'success',
          confirmButtonText: `Cerrar`,
        });
      } else {
        Swal.fire({
          title: 'UPS!',
          text: 'Hubo un error interno',
          icon: 'error',
          confirmButtonText: 'Cerrar',
        });
      }
    });
  }

  delete(): void{
    Swal.fire({
      title: '¿Desea eliminar la cuenta?',
      text: 'Esta acción es irreversible!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor: '#d9534f',
      confirmButtonText: 'Eliminar',
      cancelButtonColor: '#007bff',
      cancelButtonText: 'Cerrar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.userService.delete(String(localStorage.getItem('userID'))).subscribe( data => {
          if (data.success && data.message === 'operación exitosa') {
            localStorage.removeItem('currentUser');
            localStorage.removeItem('accessToken');
            localStorage.removeItem('userID');
            window.location.replace('login');
          } else {
            Swal.fire({
              title: 'UPS! la cuenta no se elimino',
              text: 'Hubo un error interno',
              icon: 'error',
              confirmButtonText: 'Cerrar',
            });
          }
        });
      }
    });
  }
}
