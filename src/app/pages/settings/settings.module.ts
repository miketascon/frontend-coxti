import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SettingsRoutingModule } from './settings.routing';
import { UserService } from '../services/user.service';



@NgModule({
  declarations: [SettingsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SettingsRoutingModule
  ],
  exports: [SettingsComponent],
  providers: [UserService]
})
export class SettingsModule { }
