import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-rangos',
  templateUrl: './rangos.component.html',
  styleUrls: ['./rangos.component.css']
})
export class RangosComponent implements OnInit {

  public salario = '0';
  public rangosconsulta: any;
  public misalario = '0';
  public rangos: any;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.findId(String(localStorage.getItem('userID'))).subscribe(data => {
      console.log(data);
      this.misalario = String(data.results[0].salario);
    });
  }

  misrangos(): void {
    this.userService.findRangos(this.misalario).subscribe(data => {
      this.rangos = data;
    });
  }

  consultar(): void {
    this.userService.findRangos(this.salario).subscribe(data => {
      this.rangosconsulta = data;
    });
  }

}
