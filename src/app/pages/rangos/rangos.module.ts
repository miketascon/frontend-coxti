import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RangosComponent } from './rangos.component';
import { FormsModule } from '@angular/forms';
import { RangosRoutingModule } from './rangos.routing';
import { UserService } from '../services/user.service';



@NgModule({
  declarations: [RangosComponent],
  imports: [
    CommonModule,
    FormsModule,
    RangosRoutingModule
  ],
  exports: [RangosComponent],
  providers: [UserService]
})
export class RangosModule { }
