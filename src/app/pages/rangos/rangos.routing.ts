import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RangosComponent } from './rangos.component';



const routes: Routes = [
  {
    path: '',
    component: RangosComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RangosRoutingModule {}
