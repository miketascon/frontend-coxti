import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfilComponent } from './perfil.component';
import { PerfilRoutingModule } from './perfil.routing';
import { UserService } from '../services/user.service';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [PerfilComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PerfilRoutingModule
  ], exports: [PerfilComponent],
  providers: [UserService]
})
export class PerfilModule { }
