import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

import { UserService } from '../services/user.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css'],
})
export class PerfilComponent implements OnInit {
  private userID = '';
  public form: FormGroup;

  constructor(
    private userService: UserService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.userID = String(localStorage.getItem('userID'));
    this.form = this.fb.group({
      nombres: ['', [Validators.required, Validators.minLength(2)]],
      apellidos: ['', [Validators.required, Validators.minLength(2)]],
      email: [{value: '', disabled: true}, [Validators.required, Validators.email]],
      identificacion: ['', []],
      celular: ['', []],
      departamento: ['', []],
      ciudad: ['', []],
      barrio: ['', []],
      direccion: ['', []],
      salario: ['', []],
      otros_ingresos: ['', []],
      gastos_mensuales: ['', []],
      gastos_financieros: ['', []],
    });
  }

  ngOnInit(): void {
    this.userService.findId(this.userID).subscribe((user) => {
      if (user.message === 'query user id') {
        this.form.patchValue({nombres: user.results[0].nombres});
        this.form.patchValue({apellidos: user.results[0].apellidos});
        this.form.patchValue({email: user.results[0].email});
        this.form.patchValue({identificacion: user.results[0].identificacion});
        this.form.patchValue({celular: user.results[0].celular});
        this.form.patchValue({departamento: user.results[0].departamento});
        this.form.patchValue({ciudad: user.results[0].ciudad});
        this.form.patchValue({barrio: user.results[0].barrio});
        this.form.patchValue({direccion: user.results[0].direccion});
        this.form.patchValue({salario: user.results[0].salario});
        this.form.patchValue({otros_ingresos: user.results[0].otros_ingresos});
        this.form.patchValue({gastos_mensuales: user.results[0].gastos_mensuales});
        this.form.patchValue({gastos_financieros: user.results[0].gastos_financieros});

      } else if (user.message === 'Token no válido') {
        Swal.fire({
          title: 'UPS!',
          text: 'Su sesión ha expirado',
          icon: 'error',
          confirmButtonText: `Cerrar`,
        }).then((result) => {
          localStorage.removeItem('accessToken');
          localStorage.removeItem('currentUser');
          localStorage.removeItem('userID');
          this.router.navigate(['login']);
        });
      }
    });
  }

    // tslint:disable-next-line:typedef
    get f() {
      return this.form.controls;
    }

  send(): void {
    this.userService.update(this.form.value, String(localStorage.getItem('userID'))).subscribe( data => {
      if (data.success && data.message === 'operación exitosa') {
        Swal.fire({
          title: 'Felicitacione!',
          text: data.message,
          icon: 'success',
          confirmButtonText: `Cerrar`,
        });
      } else {
        Swal.fire({
          title: 'UPS!',
          text: 'Hubo un error interno',
          icon: 'error',
          confirmButtonText: 'Cerrar',
        });
      }
    });
  }
}
