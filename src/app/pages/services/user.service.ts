import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public headers: any;

  constructor(private http: HttpClient) {
    if (localStorage.getItem('accessToken')) {
      this.headers = new HttpHeaders().set('token', String(localStorage.getItem('accessToken')));
    } else {
      this.headers = new HttpHeaders().set('token', 'token');
    }
  }

  findAll(): Observable<User> {
    return this.http.get<User>(`${environment.url}users`, {
      headers: this.headers,
    });
  }

  findId(id: string): Observable<User> {
    return this.http.get<User>(`${environment.url}users/?id=${id}`, {
      headers: this.headers,
    });
  }


  // tslint:disable-next-line:variable-name
  findRangos(salario: string): Observable<User> {
    return this.http.get<User>(`${environment.url}users/aleatorio/?number=${salario}`, {
      headers: this.headers,
    });
  }

  update(user: User, id: string): Observable<User> {
    return this.http.put<User>(
      `${environment.url}users/?id=${id}`,
      { user },
      { headers: this.headers }
    );
  }

  updatePassword(password: string, id: string): Observable<User> {
    return this.http.put<User>(
      `${environment.url}users/updatepass/?id=${id}`,
      { password },
      { headers: this.headers }
    );
  }

  delete(id: string): Observable<User> {
    return this.http.delete<User>(`${environment.url}users/` + id, {
      headers: this.headers,
    });
  }
}
