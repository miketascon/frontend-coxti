import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { LayoutModule } from '../layout/layout.module';
import { PagesRoutingModule } from './pages.routing';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [PagesComponent],
  imports: [
    CommonModule,
    LayoutModule,
    PagesRoutingModule,
    HttpClientModule
  ]
})
export class PagesModule { }
