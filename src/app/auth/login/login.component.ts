import { Component, OnInit } from '@angular/core';
import {  FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { AuthService } from '../services/auth.service';

declare const $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  public anio = 0;
  public form: FormGroup;

  constructor(private fb: FormBuilder, private auth: AuthService) {
    this.form = this.fb.group({
      email: [
        '',
        [
          Validators.required,
          Validators.email
        ],
      ],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  ngOnInit(): void {
    this.anio = new Date().getFullYear();
    $('body').addClass('bg-primary');
  }

  // tslint:disable-next-line:typedef
  get f() { return this.form.controls; }

  login(): void {
    this.auth.login(this.form.value.email, this.form.value.password).subscribe( data => {
      console.log(data);
      if (data.success && data.message === 'login') {
        localStorage.setItem('accessToken', String(data.token));
        localStorage.setItem('currentUser', JSON.stringify(data.results[0]));
        localStorage.setItem('userID', String(data.results[0].id));
        window.location.replace('/pages/perfil');
      } else  {
        Swal.fire({
          title: 'UPS!',
          text: data.message,
          icon: 'error',
          confirmButtonText: 'Cerrar'
        });
      }
    });
  }
}
