import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/models/user';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<User> {
    return this.http.post<User>(`${environment.url}users/login`, { email, password});
  }

  register(nombres: string, apellidos: string, email: string, password: string): Observable<User> {
    return this.http.post<User>(`${environment.url}users/`,  {nombres, apellidos, email, password});
  }

}
