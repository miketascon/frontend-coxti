import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

declare const $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
  public anio = 0;
  public form: FormGroup;

  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) {
    this.form = this.fb.group({
      nombres: ['', [Validators.required, Validators.minLength(2)]],
      apellidos: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      passwordConfirm: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  ngOnInit(): void {
    this.anio = new Date().getFullYear();
    $('body').addClass('bg-primary');
  }

  // tslint:disable-next-line:typedef
  get f() {
    return this.form.controls;
  }

  register(): void {
    this.auth
      .register(
        this.form.value.nombres,
        this.form.value.apellidos,
        this.form.value.email,
        this.form.value.password
      )
      .subscribe((data) => {
        console.log(data);

        if (data.success && data.message === 'operación exitosa') {
          Swal.fire({
            title: 'Felicitacione!',
            text: data.message,
            icon: 'success',
            confirmButtonText: `Cerrar`,
          }).then((result) => {
            this.router.navigate(['login']);
          });
        } else if (data.err?.code === 'ER_DUP_ENTRY') {
          Swal.fire({
            title: 'UPS!',
            text: 'usuario existente',
            icon: 'error',
            confirmButtonText: 'Cerrar',
          });
        } else {
          Swal.fire({
            title: 'UPS!',
            text: 'hubo un error',
            icon: 'error',
            confirmButtonText: 'Cerrar',
          });
        }
      });
  }
}
